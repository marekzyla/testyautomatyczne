﻿
using Xamarin.Forms;

namespace TestyAutomatyczne.Views
{
    public partial class CalculatorPage : ContentPage
    {
        public CalculatorPage()
        {
            InitializeComponent();
        }

        private void ButtonCalculate_Clicked(object sender, System.EventArgs e)
        {
            int result = int.Parse(EntryNumberA.Text) + int.Parse(EntryNumberB.Text);
            LabelEqual.Text = result.ToString();

        }
    }
}