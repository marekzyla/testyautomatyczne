﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestyAutomatyczne.Views
{
   
    public partial class TextChanger : ContentPage
    {
        public TextChanger()
        {
            InitializeComponent();
        }

        private void Entry_Focused(object sender, FocusEventArgs e)
        {
            EntryText.Text = null;
        }

        private void ButtonChange_Clicked(object sender, EventArgs e)
        {
            LabelBeforeTextChange.Text = EntryText.Text;
            string changed="";
            for (int i = 1; i <= EntryText.Text.Length; i++)
            {
                changed += EntryText.Text[EntryText.Text.Length - i];
            }

            LabelTextChanged.Text = changed;
            
        }
    }
}