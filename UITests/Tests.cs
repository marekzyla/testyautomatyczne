﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace UITests
{
    [TestFixture(Platform.Android)]
    //[TestFixture(Platform.iOS)]

    public class Tests
    {
        IApp app;
        Platform platform;

        public Tests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen.");

            //app.Back();
        }
        [Test]
        public void TextChanging(
            [Values("Rumbumber")] string test)
        {
            //app.Tap(x => x.Marked("About"));
            app.Tap("Changer");
            app.EnterText("EntryText", test);
            string changed = "";
            for (int i = 1; i <= test.Length; i++)
            {
                changed += test[test.Length - i];
            }
            test = changed;
            app.Tap("ButtonChange");
            var actual = app.Query(t => t.Marked("TextChanged")).First().Text;
            Assert.AreEqual(test, actual);
        }
        [Test]
        public void CellButtonsWorking()
        {

            var result = app.Query("LabelTextAutomation");
            /*.Where(x => x.Text.EndsWith("s"));*/
            foreach (var item in result)
            {
                app.Tap(item.Text);
                app.Back();
            }

            //app.Tap(x => x.Marked("ItemsListViewAutomation"));
        }
        [Test]

        public void EntryA_And_B_Returns_Proper_Value(
            [Values(10, 20)] int x, [Values(40, 50)] int y
            )
        {
            app.Tap("Calculator");

            app.ClearText("EntryNumberA");
            app.EnterText("EntryNumberA", x.ToString());
            app.ClearText("EntryNumberB");
            app.EnterText("EntryNumberB", y.ToString());
            app.Tap("ButtonCalculate");

            var actual = app.Query(t => t.Marked("LabelEqual")).First().Text;
            //var actual = app.Query(c=>c.Text("LabelEqual"));

            Assert.AreEqual((x + y), int.Parse(actual));
        }



    }
}

